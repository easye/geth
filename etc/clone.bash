#!/usr/bin/env bash

work=${HOME}/work
gopath=${work}/go
gopath_src=${gopath}/src

git_uri_scheme="https"
git_uri_authority="github.com"
git_uri_path="ethereumproject/go-ethereum/"
git_uri="${git_uri_scheme}://${git_uri_authority}/${git_uri_path}"
git_version="v4.1.0"

geth_src="${gopath_src}/${git_uri_authority}/$(dirname ${git_uri_path})"

mkdir -p ${geth_src}
cd ${geth_src}
dir=$(basename ${git_uri})
git clone ${git_uri} ${dir}
cd ${dir}
git checkout ${git_version}


