#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
. ${DIR}/env.bash

# TODO: replace me with some real URI parsing code
git_uri_scheme="https"
git_uri_authority="github.com"
git_uri_path="ethereum/go-ethereum/"
git_uri="${git_uri_scheme}://${git_uri_authority}/${git_uri_path}"
git_version="v1.7.3"

geth_src="${gopath_src}/${git_uri_authority}/$(dirname ${git_uri_path})"
dir=$(basename ${git_uri})

dest=${geth_src}/${dir}

if [ ! -d ${dest} ]; then 
    mkdir -p ${geth_src}
    cd ${geth_src}
    git clone ${git_uri} ${dir}
else
    cd ${dest}
    git fetch
fi 

cd ${dest}
git checkout ${git_version}


