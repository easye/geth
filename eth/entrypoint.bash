#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

. ${DIR}/env.bash

cmd="exec ${gopath}/bin/geth --datadir ${geth_d_root}"

if [[ $# -eq 0 ]]; then
    $cmd 2>&1 | tee ${var_log}/geth.out
else
    $cmd attach ipc:${geth_d_root}/geth.ipc $*
fi

   


