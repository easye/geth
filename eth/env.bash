#!/usr/bin/env bash

# N.b. variables apparently not interpolated by docker-compose

d=${HOME}
profile=${d}/.profile

work=${d}/work/
gopath=${work}/go/
gopath_src=${work}/go/src/

var_d=${d}/var
geth_d_root=${var_d}/geth.d/
var_tmp=${var_d}/tmp/
var_log=${var_d}/log/
