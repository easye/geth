#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
. ${DIR}/env.bash

export GOPATH=${gopath}

. ${profile} \
 && cd ${gopath}/src/github.com/ethereum/go-ethereum/ \
 && go install -x ./cmd/...
