#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

. ${DIR}/env.bash

echo "GOPATH=${gopath}; export GOPATH" >> $profile
