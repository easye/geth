#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

. ${DIR}/env.bash

for dir in ${geth_d_root} ${var_tmp} ${var_log}; do
    mkdir -p ${dir}
done

