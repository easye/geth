#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

var_d=$HOME/var
log_d=${var_d}/log; mkdir -p $log_d
logs=${log_d}/geth.out
var_geth_d=${var_d}/geth.d; mkdir -p $var_geth_d

#cmd=$HOME/work/go-ethereum/build/bin/geth
# FIXME
cmd=~/work/go/bin/geth
#options="--jitvm=false --verbosity 5 --cache=1024"
options="--rpc"

# Use this for sycnning the initial block chain past #2.5e6
#options="--jitvm=false --verbosity 3 --cache=1024 --fast"

printf  "Backgrounding %s %s\nwith datadir %s\nlogging to %s\n" "$cmd" "$options" "$var_geth_d" "$logs"

nohup $cmd --datadir $var_geth_d $options 2>&1 >>${logs} &



